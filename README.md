# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This Repository Includes A README file which explains the procedure for the installation of Eclipse and Installing plugins for the jBPM,how to integrate jBPM Plugins with Eclipse and creating a jBPM Project,further it includes the procedure to integrate a Chat Application with jBPM Project

### How do I get set up? ###

**Installation of Eclipse Neon**
1. Download the zip from given link  https://drive.google.com/open?id=0B6PJsaW7I03OWHdCbXVwT0oyWk0
2. Unzip and Install It.
3. Install Plugins Of jBPM 5.3.0 in eclipse.
4. Download the plugins from the given link:
	https://drive.google.com/open?id=0B6PJsaW7I03OU3pmZDZvUy03N3M
5. Open Eclipse > Go to Help >Install new software >Add > Archive >Set the path to "droolsjbpm-tools-distribution-5.3.0.Final\binaries\org.drools.updatesite”
6.  Check Select All
7.  Click Next > Agree > Finish
8.  Eclipse will then restart.


**Creating a jBPM Project**
1. Click On File > Select New project > Select Drools project > Give a New Project Name > Next > Select the options (Only Last two options, Deselect above 2 options which are by default checked,as shown in figure).

![Screenshot.png](https://bitbucket.org/repo/My6jbX/images/3739278590-Screenshot.png)

2. Click Next > Click Finish
3. Drools Project is now created.